package main

import (
	"context"
	"log"
	"sync"

	"github.com/cloudwego/netpoll"
)

func main() {
	network, address := "tcp", ":8080"
	listener, _ := netpoll.CreateListener(network, address)
	eventLoop, _ := netpoll.NewEventLoop(
		handle,
		netpoll.WithOnPrepare(prepare),
	)
	eventLoop.Serve(listener)
}

var conns = struct {
	sync.Mutex
	Conns map[netpoll.Connection]struct{}
}{
	sync.Mutex{},
	map[netpoll.Connection]struct{}{},
}

func prepare(conn netpoll.Connection) context.Context {
	println("open")
	conns.Lock()
	conns.Conns[conn] = struct{}{}
	conns.Unlock()
	conn.AddCloseCallback(func(conn netpoll.Connection) error {
		println("close")
		conns.Lock()
		delete(conns.Conns, conn)
		conns.Unlock()
		return nil
	})
	return context.Background()
}

func handle(ctx context.Context, conn netpoll.Connection) error {
	reader := conn.Reader()
	defer reader.Release()
	msg, err := reader.ReadString(reader.Len())
	if err != nil {
		log.Println(err)
		return err
	}
	//print(msg)
	for conn := range conns.Conns {
		writer := conn.Writer()
		_, err = writer.WriteString(msg)
		if err == nil {
			err = writer.Flush()
		}
		if err != nil {
			log.Println(err)
			return err
		}
	}
	return nil
}
