package main

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/cloudwego/netpoll"
)

func handle(_ context.Context, conn netpoll.Connection) error {
	reader := conn.Reader()
	defer reader.Release()
	msg, err := reader.ReadString(reader.Len())
	if err != nil {
		log.Fatalln(err)
	}
	print("> ", msg)
	return nil
}
func main() {
	network, address, timeout := "tcp", "127.0.0.1:8080", time.Second
	for {
		conn, err := netpoll.DialConnection(network, address, timeout)
		if err != nil {
			println(err.Error())
			return
		}
		defer conn.Close()
		conn.SetOnRequest(handle)
		writer := conn.Writer()
		bs := make([]byte, 99)
		for err == nil {
			n, _ := os.Stdin.Read(bs)
			if _, err = writer.WriteBinary(bs[:n]); err == nil {
				err = writer.Flush()
			}
		}
	}
}
